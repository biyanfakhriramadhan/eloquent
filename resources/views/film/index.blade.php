@extends('layouts.master')

@section('content')

@section('header', 'Halaman Index')

<section class="content">
    <div class="card">
    <div class="card-header">
        <h3 class="card-title">halaman form</h3>

        <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
        </button>
        </div>
    </div>
    <div class="card-body">
<a href="/film/create" class="btn btn-primary mb-3">Tambah</a>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Ringkasan</th>
                <th scope="col">Tahun</th>
                <th scope="col">Poster</th>
            </tr>
            </thead>
            <body>
                @forelse ($film as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->ringkasan}}</td>
                        <td>{{$value->tahun}}</td>
                        <td>{{$value->poster}}</td>
                        <td>
                            <a href="/film/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/film/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/film/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
    </div>
    <!-- /.card -->

    </section>

@endsection
