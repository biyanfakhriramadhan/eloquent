@extends('layouts.master')

@section('content')

@section('header', 'Halaman Show')

<section class="content">
    <div class="card">
    <div class="card-header">
        <h3 class="card-title">halaman Show</h3>

        <div class="card-tools">
        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
        </button>
        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
        </button>
        </div>
    </div>
    <div class="card-body">
<h2>Show Cast {{$film->id}}</h2>
<h4>{{$film->judul}}</h4>
<p>{{$film->ringkasan}}</p>
<p>{{$film->tahun}}</p>
<p>{{$film->poster}}</p>
</div>
<!-- /.card-body -->
<div class="card-footer">
    Footer
</div>
<!-- /.card-footer-->
</div>
<!-- /.card -->

</section>
@endsection
