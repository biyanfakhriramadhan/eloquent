<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use PHPUnit\TextUI\XmlConfiguration\Group;

Route::get('/', 'IndexController@index');

Route::get('/data-tables', 'IndexController@data_table');


Route::get('/register2', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function () {
    return view('layouts.master');
});


Route::group([
    'prefix' => 'cast',
    'as' => 'cast.'
], function() {


    Route::get('/', 'CastController@index')->name('index');
    Route::get('/create', 'CastController@create')->name('create');
    Route::post('/', 'CastController@store')->name('store');
    Route::get('/{cast}', 'CastController@show')->name('show');
    Route::get('/{cast}/edit', 'CastController@edit')->name('edit');
    Route::put('/{cast}', 'CastController@update')->name('update');
    Route::delete('/{cast}', 'CastController@destroy')->name('destroy');

});
Route::group([
    'prefix' => 'film',
    'as' => 'film.'
], function() {


    Route::get('/', 'FilmController@index')->name('index');
    Route::get('/create', 'FilmController@create')->name('create');
    Route::post('/', 'FilmController@store')->name('store');
    Route::get('/{film}', 'FilmController@show')->name('show');
    Route::get('/{film}/edit', 'FilmController@edit')->name('edit');
    Route::put('/{film}', 'FilmController@update')->name('update');
    Route::delete('/{film}', 'FilmController@destroy')->name('destroy');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
