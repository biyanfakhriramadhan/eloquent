<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Genre extends Model
{
    protected $guarded = [];

    public function films()
    {
        return $this->HasMany(Film::class);
    }
}
