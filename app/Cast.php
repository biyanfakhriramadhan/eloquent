<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $guarded = [];

    public function perans()
    {
        return $this->hasMany(Peran::class);
    }
}
